// JSON - lightweight - dis a data format
// for transmitting data to backend from frontend.
// diff to object syntax si json format may double quote sa properties.
// JS => JSON.stringify => Application 
// JS <= JSON.parse <= Application 

/**
    JSON
        JSON - stands for javascript object notation
        JSON - is also used in other programming lang hence the name Javascript

    Syntax:
        {
            "propertyA": "valueA",
            "propertyB": "valueB",
        }
 */

    // let JSON Object
    // {
    //     "city": "quezon city",
    //     "province": "metro manila",
    //     "country": "philippines"
    // }

    // // JSON Array
    // "cities" = [
    //     "city": "Quezon City", "province": "Metro Manila", "country": "Philippines", 
    //     "city": "Quezon City", "province": "Metro Manila", "country": "Philippines",
    //     "city": "Quezon City", "province": "Metro Manila", "country": "Philippines",
    // ]

// JSON Methods
    //The JSON object contains methods for parsing and converting data into a stringified JSON.
    
    // Converting Data into a stringified JSON

    // javascript object
    let batchesArr = [
        { batchName: "Batch 189" },
        { batchName: "Batch 190" },
    ]
    console.log(batchesArr);
    /*
    The stringify method ti is use to convert JS objects into a string.
        Before sending data, we convert an array or an object to its string equivalent
        with the use of stringify.
     */ 
    console.log();
    console.log("Result from stringify method");
    console.log(JSON.stringify(batchesArr));

    
    let data = JSON.stringify({
        name: "John",
        age: 31,
        address: {
            city: "Manila",
            country: "Philippines"
        }
    })
    console.log("yoyo");
    console.log(data);

    // User Details
    // let firstName = prompt("what is your first name?");
    // let lastName = prompt("what is your last name?");
    // let age = prompt("what is your age?");
    // let address = {
    //     city: prompt("which city do you live in?"),
    //     country: prompt("which country does your city address belong to?")
    // }

    // let otherData = JSON.stringify({
    //     firstName: firstName,
    //     lastName: lastName,
    //     age: age,
    //     address: address
    // })

    // console.log(otherData);

    // Convert stringified JSON to JS objects
        // JSON.parse()

    let batchesJSON = `[
        {
            "batchName": "Batch 189"
        },
        {
            "batchName": "Batch 190"
        }
    ]`
    console.log();
    console.log(batchesJSON);
    // Upon receiving data, the JSON text can be converted to JS object so that we can use it in our program.
    console.log("Result from parse method:");
    console.log(JSON.parse(batchesJSON));

    let stringifiedObject = `{
        "name": "Ivy",
        "age": "18",
        "address": {
            "city": "Caloocan City",
            "country": "Philippines"
        }
       
    }`

    console.log(stringifiedObject);
    console.log(JSON.parse(stringifiedObject));